(function(){	
	'use strict'
	const btn = document.querySelector('.main-header__more ');
	const anchor = document.getElementById('content');
	
	btn.onclick = function(e) {
		e.preventDefault();
		anchor.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  };
})()